
# Software Engineering Assignment

## 1 Introduction

The purpose of this assignment is to try and provide a relatively realistic example of at least one of the tasks that you would be likely to do in the lab.

It is based on a public dataset collected by Claire Gillan and colleagues (Gillan et al., 2016: 10.7554/eLife.11305 of the choices that a large number of online participants made in a simple ’game’ (illustrated in figure 1a of the paper).

On each trial (out of 200), participants had to make two successive choices between pairs of fractal images in order to try and win binary rewards available after the second choice. The probability that the bottom (’stage-2’) fractals provided reward drifted according to reflecting random walks (figure 1b) so subjects might have continually to revise their choices. Critically, the first (’stage-1’) choice had a stochastic consequence in terms of which of two pairs of fractal patterns would be available for the second choice. For each first choice, one pair was the common consequence (70% probability); the other rare (30% probability).

The task was intended to dissociate two sorts of control. In so-called model-free control, delivery of a reward after the second choice directly strengthens or reinforces the first choice. In
model-based control, there is an interaction with the consequence of the first choice - since, if the
subject wants to get access to one particular bottom level fractal on the next trial, then they need
to take account of whether the transition was common or rare (see figures 1c;d). In reality, people combine both sorts of control - to different degrees.

We would like you to write code to fit the formal account of this sort of combined controller that Gillan et al., (2016) describe on P19-20 of the paper to the behaviour of the 253 subjects that is available in this github archive. We chose this subsample of subjects since they performed all trials, implying there are no missing data. The model has 6 free parameters to fit.

You are welcome to do maximum likelihood fitting of each subject; or exact or approximate fitting of a Bayesian hierarchical mixed- or random-effects model.

Please provide your code and your fitted parameters (and population parameters, if you build an hierarchical model) in a github repository. Please also provide a brief report of how you went about doing this task.
Feel free to ask questions along the way if you encounter any problems.

Some basic info for reading in and analysing the task data for the two-step task.
Note:
- Task data starts the row after the one that lists “twostep instruct 9” in column C.
- Subjects can have varying number of rows prior to the start of the task due to their repeating the instructions (having multiple instances of instructionLoop):

Task data column names:\
A = trial_num\
B = drift 1 (probability of reward after second stage option 1)\
C = drift 2 (probability of reward after second stage option 2)\
D = drift 3 (probability of reward after second stage option 3)\
E = drift 4 (probability of reward after second stage option 4)\
F = stage 1 response (left/right) \
G = stage 1 selected stimulus (1/2 - note this is redundant with the response as the stage 1 options do not switch locations)\
H = stage 1 RT\
I = transition (common = TRUE; rare = FALSE)\
J = stage 2 response (left/right)\
K = stage 2 selected stimulus (1/2 - note this is redundant with response as the stage 2 options also do not switch locations)\
L =  stage 2 state (identity 2 or 3)\
M = stage 2 RT\
N = reward (1= yes; 0=no)\
O = redundant task variable, always set to 1\
